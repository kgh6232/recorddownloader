﻿using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MC_Downloader_WPF
{
    class Util
    {
        public static bool MakeDirectory(string strDir)
        {
            try
            {
                DirectoryInfo dInfo = new DirectoryInfo(strDir);
                if (dInfo.Exists == false)
                    dInfo.Create();
            }
            catch
            {
                return false;
            }

            return true;
        }


        public static string GetTimeString(int nTime)
        {
            int nHour = nTime / 3600;
            int nMin = (nTime % 3600) / 60;
            int nSec = nTime % 60;

            return String.Format("{0:D2}:{1:D2}:{2:D2}", nHour, nMin, nSec);
        }

        public static string GetSizeString(ulong nByte)
        {
            string strSize;
            ulong nTmpSize = nByte;
            int nUnit = 0;
            while (true)
            {
                if ((nTmpSize / Constants.SIZE_1K) > 0)
                {
                    ++nUnit;
                    nTmpSize = nTmpSize / Constants.SIZE_1K;
                    continue;
                }
                break;
            }

            strSize = string.Format("{0}{1}", nTmpSize, Constants.strDataUnitList[nUnit]);
            return strSize;
        }

        public static string GetJsonFormat(Dictionary<string, object> dic)
        {
            string strJson = JsonConvert.SerializeObject(dic, Formatting.Indented);

            return strJson;
        }
    }
}
