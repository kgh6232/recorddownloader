﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC_Downloader_WPF
{
    static class Constants
    {
        public const string PROGRAM_NAME = "MC_Downloader";

        /* Argument Info */
        public const string ARGUMENT_HDR = "mcdownloader:";
        public const string ARGUMENT_KEY = "listId=";
        public const string ARGUMENT_URL = "url=";

        /* Size */
        public const int SIZE_1K = 1024;
        public const int SIZE_2K = SIZE_1K * 2;
        public const int SIZE_4K = SIZE_2K * 2;

        /* Data Unit */
        public static string[] strDataUnitList = { "Byte", "KB", "MB", "GB", "TB" };

        /* Status */
        public const int STATUS_N_NONE = 0;
        public const int STATUS_N_START = 1;
        public const int STATUS_N_PAUSE = 2;
        public const int STATUS_N_STOP = 3;
        public const int STATUS_N_DONE = 4;
        public const int STATUS_N_ERROR = 99;

        public const string STATUS_ITEM_S_NONE = "-";
        public const string STATUS_ITEM_S_WAIT = "대기";
        public const string STATUS_ITEM_S_DOWN_COMPL = "다운로드 완료";
        public const string STATUS_ITEM_S_DOWN_FAIL = "다운로드 실패";
        public const string STATUS_ITEM_S_CNVT_COMPL = "변환 완료";
        public const string STATUS_ITEM_S_CNVT_FAIL = "변환 실패";
        public const string STATUS_ITEM_S_EXCEPT = "제외";

        /* Restful API Info */
        public const int RESTAPI_CALL_N_PORT = 8443;
        public const string RESTAPI_CALL_S_GET_KEY_RESOURCE = "/gtadm/v1.1/advice/getRecDownListInfo";
        public const string RESTAPI_CALL_S_GET_VOC_LIST = "/gtadm/v1.1/advice/getRecDownList";
        public const string RESTAPI_CALL_S_REQ_RESULT_KEY = "listId";
        public const string RESTAPI_CALL_S_RES_RESULT_CODE = "resultCode";
        public const string RESTAPI_CALL_S_RES_RESULT_MSG = "resultMsg";
        public const string RESTAPI_CALL_S_RES_RESULT_RES = "response";
        public const string RESTAPI_CALL_S_RES_RESULT_RES_AUTHKEY = "authKey";
        public const string RESTAPI_CALL_S_RES_RESULT_RES_PARAM = "param";
        public const string RESTAPI_CALL_S_RES_RESULT_RES_KIND = "kind";
        public const string RESTAPI_CALL_S_RES_RESULT_REQ_AUTHKEY = "Authorization";

        /* Download voc server info */
        public const int DOWNLOAD_VOC_N_PORT = 9005;

        /* Download voc file Result */
        public const int DOWNLOAD_VOC_RESULT_SUCCESS = 0;
        public const int DOWNLOAD_VOC_RESULT_UNKNOWN = 1;
        public const int DOWNLOAD_VOC_RESULT_INVALID_ARGUMENT = 2;
        public const int DOWNLOAD_VOC_RESULT_WRONG_ORDER = 3;
        public const int DOWNLOAD_VOC_RESULT_GET_LIST_FAIL = 4;
        public const int DOWNLOAD_VOC_RESULT_REC_COUNT_ZERO = 5;
        public const int DOWNLOAD_VOC_RESULT_SOCK_ERROR = 6;
        public const int DOWNLOAD_VOC_RESULT_CONNECT_FAIL = 7;
        public const int DOWNLOAD_VOC_RESULT_SEND_FAIL = 8;
        public const int DOWNLOAD_VOC_RESULT_RECIVE_FAIL = 9;
        public const int DOWNLOAD_VOC_RESULT_WAIT_TIMEOUT = 10;
        public const int DOWNLOAD_VOC_RESULT_FILEOPEN_REQ_FAIL = 11;

        /*  Convert Voc File Result*/
        public const int CONVERT_VOC_RESULT_SUCCESS = 0;
        public const int CONVERT_VOC_RESULT_CODEC_ERROR = 1;
        public const int CONVERT_VOC_RESULT_CANNOT_OPEN_VOC = 2;
        public const int CONVERT_VOC_RESULT_CANNOT_FIND_CODEC = 3;
        public const int CONVERT_VOC_RESULT_CANNOT_OPEN_DST_FILE = 4;
        public const int CONVERT_VOC_RESULT_READ_FAIL = 5;
        public const int CONVERT_VOC_RESULT_CONVERT_FAIL = 6;
        public const int CONVERT_VOC_RESULT_WRITE_FAIL = 7;
        public const int CONVERT_VOC_RESULT_UNKNOWN = 99;

        /* Codec */
        public const int CONVERT_VOC_AUDIO_TYPE_MP3 = 0;
        public const int CONVERT_VOC_AUDIO_TYPE_PCM = 1;
        public const int CONVERT_VOC_AUDIO_TYPE_ALAW = 2;
        public const int CONVERT_VOC_AUDIO_TYPE_G723_1 = 3;
    }

    public class DataGridVocSet
    {
        private static List<DataGridVocSet> instance;
        public static List<DataGridVocSet> GetInstance()
        {
            if (instance == null)
                instance = new List<DataGridVocSet>();
            return instance;
        }

        public bool IsSelected { get; set; }
        public int nIndex { get; set; }
        public string strTenantID { get; set; }
        public Int64 nAgentID { get; set; }
        public string strAgentLoginId { get; set; }
        public string strAgentDN { get; set; }
        public string strUploadDate { get; set; }
        public string strFileName { get; set; }
        public string strVocName { get; set; }
        public string strVocPath { get; set; }
        public string strAudioName { get; set; }
        public string strAudioPath { get; set; }
        public string strAudioSize { get; set; }
        public string strStatus { get; set; }
        public bool bEnable { get; set; }
    }

    public class CallApiArgSet
    {
        public string strURI;
        public string strMethod;
        public string strContentType;
        public Dictionary<string, string> dicHdr = new Dictionary<string, string>();
        public string strBody;
    }

    public class SearchRequirement
    {
        public int  nResult;
        public bool bResult;
        public string strAuthKey;
        public string strParam;
        public int nKind;
    }

    public class VocList
    {
        public VocList() { bResult = false; }
        public int nResult;
        public bool bResult;
        public List<VocInfo> listVoc;
    }

    public class VocInfo
    {
        public string strTenantID;
        public string strFileName;
        public string strUploadDate;
        public Int64 nAgentID;
        public string strAgentLoginId;
        public string strAgentDN;
    }
}
