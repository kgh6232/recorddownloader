﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Threading;
using MessageBox = System.Windows.MessageBox;
using System.Threading;
using System.Collections.Generic;

namespace MC_Downloader_WPF
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MC_Downloader : Window
    {
        public MC_Downloader(string[] strArgs)
        {
            mLogWriter = new LogWriter();
            if(!mLogWriter.Init(System.Windows.Forms.Application.StartupPath + "\\log"))
                Console.WriteLine("* MC_Downloader::constructor Failed to init LogWriter");
            
            if (strArgs.Length > 0)
                mLogWriter.Write("MC_Downloader::constructor (" + strArgs[0] + ")");
            else
                mLogWriter.Write("MC_Downloader::constructor No argument");

            if (!Init(strArgs))
                    mLogWriter.Write("MC_Downloader::constructor Init Failed");

            InitializeComponent();

            eChnglblCnt += new ChnglblCnt(SetLblCountText);
            eChngFStatus += new ChngFStatus(SetFStatus);
            eChngProgress += new ChngProgress(SetProgress);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            mLogWriter.Write("MC_Downloader::Window_Loaded");

            // Check dulpication run
            if (!CheckDuplicationRun())
            {
                mLogWriter.Write("MC_Downloader::constructor Duplicate running. Exit the program.");
                MessageBox.Show("프로그램이 이미 실행중 입니다.", "에러");
                this.Close();
            }

            // Check argument
            if (mdicRunArgSet.Count <= 0)
            {
                mLogWriter.Write("MC_Downloader::Window_Loaded Failed to load URL list.");
                MessageBox.Show("리스트를 불러오는데 실패했습니다.\n프로그램을 다시 실행해 주세요.", "에러");
                this.Close();
            }

            ClearValue(SizeToContentProperty);
            LayoutRoot.ClearValue(WidthProperty);
            LayoutRoot.ClearValue(HeightProperty);

            tbSaveDir.Text = mstrSaveDir;

            // Get voc info list
            Notice_Window noticeWdw = new Notice_Window();
            noticeWdw.Show();

            // get key
            SearchRequirement stSchReqmt = RunGetConditionAPI();
            if (stSchReqmt.bResult == false)
            {
                noticeWdw.Close();
                mLogWriter.Write("MC_Downloader::Window_Loaded Failed to call restapi.");
                MessageBox.Show("리스트를 불러오는데 실패했습니다.\n프로그램을 다시 실행해 주세요.", "에러");
                this.Close();
                return;
            }

            // get voc list
            VocList vocList = RunGetVocListAPI(stSchReqmt);
            if (vocList.bResult == false)
            {
                noticeWdw.Close();
                mLogWriter.Write("MC_Downloader::Window_Loaded Failed to call restapi.");
                MessageBox.Show("리스트를 불러오는데 실패했습니다.\n프로그램을 다시 실행해 주세요.", "에러");
                this.Close();
                return;
            }

            noticeWdw.Close();

            // Set list to form
            for (int i = 0; i < vocList.listVoc.Count; ++i)
            {
                VocInfo vocInfo = vocList.listVoc.ElementAt(i);
                string strVocFilePath = vocInfo.strUploadDate + "_" + vocInfo.strAgentDN + ".voc";
                string strAudioFilePath = vocInfo.strUploadDate + "_" + vocInfo.strAgentDN;
                mLogWriter.Write("MC_Downloader::Window_Loaded [" + (i + 1) + "]");
                mLogWriter.Write("MC_Downloader::Window_Loaded   > tenantid   : " + vocInfo.strTenantID);
                mLogWriter.Write("MC_Downloader::Window_Loaded   > filename   : " + vocInfo.strFileName);
                mLogWriter.Write("MC_Downloader::Window_Loaded   > dn_no      : " + vocInfo.strAgentDN);
                mLogWriter.Write("MC_Downloader::Window_Loaded   > agentId    : " + vocInfo.nAgentID);
                mLogWriter.Write("MC_Downloader::Window_Loaded   > agentLId   : " + vocInfo.strAgentLoginId);
                mLogWriter.Write("MC_Downloader::Window_Loaded   > uploaddate : " + vocInfo.strUploadDate);
                ++mnWorkCnt;
                ++mnTotalCnt;
                mnProgressBarSize += 2;
                DataGridVocSet.GetInstance().Add(new DataGridVocSet()
                {
                    IsSelected = true,
                    nIndex = i + 1,
                    strTenantID = vocInfo.strTenantID,
                    nAgentID = vocInfo.nAgentID,
                    strAgentDN = vocInfo.strAgentDN,
                    strAgentLoginId = vocInfo.strAgentLoginId,
                    strFileName = vocInfo.strFileName,
                    strUploadDate = vocInfo.strUploadDate,
                    strVocName = strVocFilePath,
                    strAudioName = Constants.STATUS_ITEM_S_NONE,
                    strAudioPath = strAudioFilePath,
                    strAudioSize = Constants.STATUS_ITEM_S_NONE,
                    strStatus = Constants.STATUS_ITEM_S_WAIT,
                    bEnable = true
                });
            }
            lvList.ItemsSource = DataGridVocSet.GetInstance();

            lblCount.Content = String.Format("00/{0:D2}", mnTotalCnt);
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            mLogWriter.Write("MC_Downloader::Window_Unloaded");
            Clear();
        }

        private void BtSearchDir_Click(object sender, RoutedEventArgs e)
        {
            mLogWriter.Write("MC_Downloader::BtSearchDir_Click");
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowDialog();

            if (fbd.SelectedPath.Length > 0)
            {
                mstrSaveDir = fbd.SelectedPath;
                tbSaveDir.Text = mstrSaveDir;
                mLogWriter.Write("MC_Downloader::BtSearchDir_Click " + mstrSaveDir);
            }
            else
            {
                mLogWriter.Write("MC_Downloader::BtSearchDir_Click Not Selected");
            }
        }

        private void ChkboxClose_Checked(object sender, RoutedEventArgs e)
        {
            mLogWriter.Write("MC_Downloader::ChkboxClose_Checked");
            mbCloseWindow = true;
        }

        private void ChkboxClose_Unchecked(object sender, RoutedEventArgs e)
        {
            mLogWriter.Write("MC_Downloader::ChkboxClose_Unchecked");
            mbCloseWindow = false;
        }

        private void ChkboxOpenDir_Checked(object sender, RoutedEventArgs e)
        {
            mLogWriter.Write("MC_Downloader::ChkboxOpenDir_Checked");
            mbOpenDir = true;
        }

        private void ChkboxOpenDir_Unchecked(object sender, RoutedEventArgs e)
        {
            mLogWriter.Write("MC_Downloader::ChkboxOpenDir_Checked");
            mbOpenDir = false;
        }

        private void BtStart_Click(object sender, RoutedEventArgs e)
        {
            mLogWriter.Write("MC_Downloader::BtStart_Click");

            if (mnStatus == Constants.STATUS_N_NONE)
            {
                if (!Util.MakeDirectory(mstrSaveDir))
                {
                    mLogWriter.Write("MC_Downloader::BtStart_Click Failed to Create directory. (" + mstrSaveDir + ")");
                    MessageBox.Show("폴더를 생성할 수 없습니다.\n다른 폴더를 선택해 주세요.", "경고");
                    return;
                }

                if (mnWorkCnt <= 0)
                {
                    mLogWriter.Write("MC_Downloader::BtStart_Click Nothing selected.");
                    MessageBox.Show("다운 받을 리스트를 선택해 주세요.", "경고");
                    return;
                }

                mTm.Start();
                mnStatus = Constants.STATUS_N_START;
                btStart.Content = "진행 중";

                hTh = new Thread(ConvtThFunc);
                hTh.Start();
                btSearchDir.IsEnabled = false;
            }
            else if (mnStatus == Constants.STATUS_N_START)
            {
                mnStatus = Constants.STATUS_N_STOP;
                btStart.Content = "중단 됨";
                mTm.Stop();
            }
            //else if (mnStatus == Constants.STATUS_N_PAUSE)
            //{
            //    mnStatus = Constants.STATUS_N_START;
            //    btStart.Content = "진행 중";
            //    mTm.Start();
            //}
            else
            {
            }
        }

        private void tm_Tick(object sender, EventArgs e)
        {
            ++mnTimer;
            lblTime.Content = Util.GetTimeString(mnTimer);
        }

        private void PbProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (pbProgress.Value == 100)
            {
                mLogWriter.Write("MC_Downloader::PbProgress_ValueChanged 100%");
                mTm.Stop();
                mnStatus = Constants.STATUS_N_DONE;
                btStart.Content = "완료";
                btStart.IsEnabled = false;
                chkboxClose.IsEnabled = false;
                chkboxOpenDir.IsEnabled = false;

                if (mbOpenDir == true)      Process.Start(mstrSaveDir);
                if (mbCloseWindow == true)  this.Close();
            }
        }

        private void btSearchDir_EnabledChanged(object sender, EventArgs e)
        {
            if (btSearchDir.IsEnabled == false)
            {
                btSearchDir.Background = Brushes.SlateGray;
            }
        }

        private void SetLblCountText(int nCnt, int nTot)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate {
                lblCount.Content = String.Format("{0:D2}/{1:D2}", nCnt, nTot);
            }));
        }

        private void SetFStatus(int nIdx, string strAudioName, string strAudioSize, string strStatus, bool bEnable = true)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate {
                DataGridVocSet dgVS = DataGridVocSet.GetInstance().ElementAt(nIdx);
                dgVS.strAudioName = strAudioName;
                dgVS.strAudioSize = strAudioSize;
                dgVS.strStatus = strStatus;
                dgVS.bEnable = bEnable;
                lvList.Items.Refresh();
            }));
        }

        private void SetProgress(double nPercent)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate {
                pbProgress.Value = nPercent;
            }));
        }

        private void TbSaveDir_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Console.WriteLine("* TbSaveDir_DataContextChanged");
            mstrSaveDir = tbSaveDir.Text;
        }

        private void lvbtnDel_Click(object sender, RoutedEventArgs e)
        {
            if (mnStatus == Constants.STATUS_N_DONE) return;

            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while (!(dep is System.Windows.Controls.ListViewItem))
            {
                try
                {
                    dep = VisualTreeHelper.GetParent(dep);
                }
                catch(Exception ex)
                {
                    mLogWriter.Write("MC_Downloader::lvbtnDel_Click Failed to get Parent Control : " + ex.ToString());
                    return ;
                }
            }
            System.Windows.Controls.ListViewItem lvItem = (System.Windows.Controls.ListViewItem)dep;
            DataGridVocSet dgVS = (DataGridVocSet)lvItem.Content;

            if (dgVS.bEnable == true)
            {
                mLogWriter.Write("MC_Downloader::lvbtnDel_Click Except (" + (dgVS.nIndex - 1) + ":" + dgVS.strVocPath + ")");
                SetFStatus(dgVS.nIndex - 1, Constants.STATUS_ITEM_S_NONE, Constants.STATUS_ITEM_S_NONE, Constants.STATUS_ITEM_S_EXCEPT, false);
                SetLblCountText(0, --mnWorkCnt);
                mnProgressBarSize -= 2; // 다운로드+변환
            }
            else
            {
                mLogWriter.Write("MC_Downloader::lvbtnDel_Click Include (" + (dgVS.nIndex - 1) + ":" + dgVS.strVocPath + ")");
                SetFStatus(dgVS.nIndex - 1, Constants.STATUS_ITEM_S_NONE, Constants.STATUS_ITEM_S_NONE, Constants.STATUS_ITEM_S_WAIT);
                SetLblCountText(0, ++mnWorkCnt);
                mnProgressBarSize += 2; // 다운로드+변환
            }
        }
    }
}
