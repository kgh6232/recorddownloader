﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Threading;
using System.Collections.Generic;

namespace MC_Downloader_WPF
{
    partial class MC_Downloader
    {
        /* Download VOC Functions */
        [DllImport("libiMCRecord.dll")]
        extern public static IntPtr iDownVocConstruct();
        [DllImport("libiMCRecord.dll")]
        extern public static void iDownVocInit(IntPtr pObj, string szDomain, int nPort);
        [DllImport("libiMCRecord.dll")]
        extern public static void iDownVocClear(IntPtr pObj);
        [DllImport("libiMCRecord.dll")]
        extern public static int iDownVocSetAuthKey(IntPtr pObj, string szAuthKey);
        [DllImport("libiMCRecord.dll")]
        extern public static int iDownVocSetConditions(IntPtr pObj, Int64 nTenantID, string szStartDate, string szEndDate, string szStartTime, string szEndTime);
        [DllImport("libiMCRecord.dll")]
        extern public static int iDownVocSetConditionStr(IntPtr pObj, string szParam, int nKind);
        [DllImport("libiMCRecord.dll")]
        extern public static int iDownVocCallApi(IntPtr pObj);
        [DllImport("libiMCRecord.dll")]
        extern public static IntPtr iDownVocGetVocList(IntPtr pObj);
        [DllImport("libiMCRecord.dll")]
        extern public static int iDownVocDownload(IntPtr pObj, string szIP, Int32 nPort, string szOutDir, string szTenantID, Int64 nAgentID, string szAgentDN, string szUploadDate, string szVocName, string szOutFileName);
        [DllImport("libiMCRecord.dll")]
        extern public static int iDownVocDownloads(IntPtr pObj, string szIP, Int32 nPort, string szOutDir);

        /* Convert VOC Functions */
        [DllImport("libiMCRecord.dll")]
        extern public static IntPtr iCnvtVocConstruct();
        [DllImport("libiMCRecord.dll")]
        extern public static void iCnvtVocInit(IntPtr pObj, string szOutDir);
        [DllImport("libiMCRecord.dll")]
        extern public static void iCnvtVocClear(IntPtr pObj);
        [DllImport("libiMCRecord.dll")]
        extern public static Int32 iCnvtVocConvertVoc(IntPtr pObj, string szVocPath, string szOutPath, UInt16 nAudioType);
        [DllImport("libiMCRecord.dll")]
        extern public static IntPtr iCnvtVocGetOutFilePath(IntPtr pObj);
        [DllImport("libiMCRecord.dll")]
        extern public static UInt64 iCnvtVocGetOutFileSize(IntPtr pObj);

        /* 변수 */
        private Dictionary<String, object> mdicRunArgSet;   // 실행인자 분석 결과
        private LogWriter       mLogWriter;         // 로그
        private Mutex           mMtx;               // 중복실행 확인용 뮤텍스
        private string          mRestApiDomain;     // API 서버 도메인
        private string          mDownVocSvrIP;      // Voc Download Server IP
        private CallApiArgSet   mstApiArg;          // Rest API 호출 정보
        private string          mstrRestApiRes;     // Rest API 호출 결과
        private IntPtr          mpDownVoc;          // Download voc dll 객체 저장
        private IntPtr          mpCnvtVoc;          // Convert voc dll 객체 저장
        private int             mnStatus;           // 상태 (0:none / 1:start / 2:pause / 3:Done / 99:Error)
        private int             mnTotalCnt;         // 전체 개수
        private int             mnWorkCnt;          // 작업할 개수
        private int             mnFinishCnt;        // 변환까지 완료한 파일 개수
        private int             mnProgressBarSize;  // 프로그레스바 전체 크기
        private string          mstrSaveDir;        // 파일 저장 디렉토리
        private bool            mbOpenDir;          // 완료 후 디렉토리 오픈 여부
        private bool            mbCloseWindow;      // 완료 후 창 종료 여부
        private int             mnTimer;            // 진행 시간
        private DispatcherTimer mTm;                // 타이머

        /* Thread */
        private Thread hTh;            // 스레드
        private delegate void ChnglblCnt(int nCnt, int nTot);  // lblCount text 변경 용 델리게이트
        private event ChnglblCnt eChnglblCnt;        // lblCount text 변경 용 델리게이트 이벤트
        private delegate void ChngFStatus(int nIdx, string strAudioName, string strAudioSize, string strStatus, bool bEnable = true);   // lvList 변경 용 델리게이트
        private event ChngFStatus eChngFStatus;      // lvList 변경 용 델리게이트 이벤트
        private delegate void ChngProgress(double nPercent);   // pnlProgress 변경 용 델리게이트
        private event ChngProgress eChngProgress;    // pnlProgress 변경 용 델리게이트 이벤트

        public bool Init(string[] args)
        {
            mLogWriter.Write("MC_Downloader::Init");
            mdicRunArgSet = new Dictionary<string, object>();
            if (args.Length > 0)
            {
                mLogWriter.Write("MC_Downloader::Init (" + args[0] + ")");
                if (!ParseArgument(args[0]))
                {
                    mLogWriter.Write("MC_Downloader::Init Failed to parse argument.");
                    return false;    // parse Json argument
                }
            }

            mnStatus = Constants.STATUS_N_NONE;
            if(Environment.GetEnvironmentVariable("USERPROFILE") != string.Empty)
                mstrSaveDir = Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\MC_Downloader";

            mTm = new DispatcherTimer();
            mTm.Interval = TimeSpan.FromSeconds(1);
            mTm.Tick += new EventHandler(tm_Tick);

            mpDownVoc = iDownVocConstruct();
            mpCnvtVoc = iCnvtVocConstruct();

            return true;
        }

        public void Clear()
        {
            mLogWriter.Close();
        }

        public bool CheckDuplicationRun()
        {
            mLogWriter.Write("MC_Downloader::CheckDuplicationRun");
            bool bRet;
            mMtx = new Mutex(true, Constants.PROGRAM_NAME, out bRet);    // 임의의 뮤텍스명. 현재 프로그램 이름
            if(bRet == false)
                mLogWriter.Write("MC_Downloader::CheckDuplicationRun Duplication Run!!!");

            return bRet;
        }

        public bool ParseArgument(string strArg)
        {
            mLogWriter.Write("MC_Downloader::ParseArgument (" + strArg + ")");
            try
            {
                // mcdownloader:listId=AQSWDEFR&url=mc.toast.com
                if(strArg.IndexOf(Constants.ARGUMENT_HDR) != 0)
                {
                    mLogWriter.Write("MC_Downloader::ParseArgument invalid argument format.");
                    return false;                    
                }
                strArg = strArg.Substring(Constants.ARGUMENT_HDR.Length);
                string[] strArgArr = strArg.Split('&');
                foreach(string arg in strArgArr)
                {
                    if (arg.IndexOf(Constants.ARGUMENT_KEY) == 0)
                    {
                        mdicRunArgSet[Constants.RESTAPI_CALL_S_REQ_RESULT_KEY] = arg.Substring(Constants.ARGUMENT_KEY.Length);
                    }
                    else if (arg.IndexOf(Constants.ARGUMENT_URL) == 0)
                    {
                        mRestApiDomain = "https://" + arg.Substring(Constants.ARGUMENT_URL.Length);

                        IPAddress[] addresses = Dns.GetHostAddresses(arg.Substring(Constants.ARGUMENT_URL.Length));
                        foreach (IPAddress address in addresses)
                        {
                            mDownVocSvrIP = address.ToString();
                            break;
                        }
                    }
                }

                mLogWriter.Write("MC_Downloader::ParseArgument argument");
                foreach (KeyValuePair<string, object> items in mdicRunArgSet)
                {
                    mLogWriter.Write("  > " + items.Key + " : " + items.Value);
                }
            }
            catch (Exception ex)
            {
                mLogWriter.Write("MC_Downloader::ParseArgument Failed to parse argument. (" + ex.ToString() + ")");
                return false;
            }

            return true;
        }

        public SearchRequirement RunGetConditionAPI()
        {
            Dictionary<string, object> dicReq = new Dictionary<string, object>();
            dicReq["request"] = mdicRunArgSet;
            mstApiArg = new CallApiArgSet
            {
                strURI = mRestApiDomain + ":" + Constants.RESTAPI_CALL_N_PORT + Constants.RESTAPI_CALL_S_GET_KEY_RESOURCE,
                strMethod = "POST",
                strContentType = "application/json",
                strBody = Util.GetJsonFormat(dicReq)
            };

            mLogWriter.Write("MC_Downloader::Window_Loaded run restapi. (" + mstApiArg.strURI + ")");
            mLogWriter.Write("MC_Downloader::Window_Loaded run restapi Body. (" + mstApiArg.strBody + ")");
            hTh = new Thread(new ParameterizedThreadStart(CallRestApiThFunc));
            hTh.Start(mstApiArg);
            hTh.Join();

            return ParseGetKeyRestApiResult(mstrRestApiRes);
        }

        public SearchRequirement ParseGetKeyRestApiResult(string strJson)
        {
            mLogWriter.Write("MC_Downloader::ParseGetKeyRestApiResult (" + strJson + ")");
            SearchRequirement stSchReqmt = new SearchRequirement();
            stSchReqmt.bResult = false;
            try
            {
                JObject jObj = JObject.Parse(strJson);

                if (jObj[Constants.RESTAPI_CALL_S_RES_RESULT_CODE] == null) return stSchReqmt;
                stSchReqmt.nResult = (int)jObj[Constants.RESTAPI_CALL_S_RES_RESULT_CODE];
                mLogWriter.Write("MC_Downloader::ParseGetKeyRestApiResult Result code : " + stSchReqmt.nResult);
                if (stSchReqmt.nResult != 0)   return stSchReqmt;

                if (jObj[Constants.RESTAPI_CALL_S_RES_RESULT_MSG] == null) return stSchReqmt;
                stSchReqmt.bResult = jObj[Constants.RESTAPI_CALL_S_RES_RESULT_MSG].ToString().Equals("SUCCESS") ? true : false;
                mLogWriter.Write("MC_Downloader::ParseGetKeyRestApiResult Result bool : " + stSchReqmt.bResult);
                if (stSchReqmt.bResult == false)  return stSchReqmt;

                if (jObj[Constants.RESTAPI_CALL_S_RES_RESULT_RES] == null) return stSchReqmt;                

                JObject jResObj = (JObject)jObj[Constants.RESTAPI_CALL_S_RES_RESULT_RES];
                if (jResObj[Constants.RESTAPI_CALL_S_RES_RESULT_RES_AUTHKEY] == null) return stSchReqmt;
                if (jResObj[Constants.RESTAPI_CALL_S_RES_RESULT_RES_PARAM] == null) return stSchReqmt;
                if (jResObj[Constants.RESTAPI_CALL_S_RES_RESULT_RES_KIND] == null) return stSchReqmt;

                stSchReqmt.strAuthKey = jResObj[Constants.RESTAPI_CALL_S_RES_RESULT_RES_AUTHKEY].ToString();
                stSchReqmt.strParam = jResObj[Constants.RESTAPI_CALL_S_RES_RESULT_RES_PARAM].ToString();
                stSchReqmt.nKind = (int)jResObj[Constants.RESTAPI_CALL_S_RES_RESULT_RES_KIND];
                mLogWriter.Write("MC_Downloader::ParseGetKeyRestApiResult Result strAuthKey : " + stSchReqmt.strAuthKey);
                mLogWriter.Write("MC_Downloader::ParseGetKeyRestApiResult Result strParam   : " + stSchReqmt.strParam);
                mLogWriter.Write("MC_Downloader::ParseGetKeyRestApiResult Result nKind      : " + stSchReqmt.nKind);
            }
            catch (Exception ex)
            {
                mLogWriter.Write("MC_Downloader::ParseGetKeyRestApiResult Failed to parse json. (" + ex.ToString() + ")");
                stSchReqmt.bResult = false;
                return stSchReqmt;
            }

            return stSchReqmt;
        }

        public VocList RunGetVocListAPI(SearchRequirement stSchReqmt)
        {
            mLogWriter.Write("MC_Downloader::RunGetVocListAPI");
            mLogWriter.Write("  > kind  : " + stSchReqmt.nKind);
            mLogWriter.Write("  > param : " + stSchReqmt.strParam);
            int nRc = Constants.DOWNLOAD_VOC_RESULT_SUCCESS;
            iDownVocInit(mpDownVoc, mRestApiDomain, Constants.RESTAPI_CALL_N_PORT);

            iDownVocSetAuthKey(mpDownVoc, stSchReqmt.strAuthKey);
            if (stSchReqmt.nKind == 0)   // 조건 다운로드
            {
                Console.WriteLine("strParam : " + stSchReqmt.strParam);
                stSchReqmt.strParam.Replace("\"", "\\\"");
                nRc = iDownVocSetConditionStr(mpDownVoc, stSchReqmt.strParam, stSchReqmt.nKind);
            }
            else if (stSchReqmt.nKind == 1)  // 선택 다운로드
            {
                nRc = iDownVocSetConditionStr(mpDownVoc, stSchReqmt.strParam, stSchReqmt.nKind);
            }
            else
            {
                return new VocList();
            }   

            nRc = iDownVocCallApi(mpDownVoc);
            mstrRestApiRes = Marshal.PtrToStringAnsi(iDownVocGetVocList(mpDownVoc));

            return ParseVocInfoList(mstrRestApiRes);
        }

        public VocList ParseVocInfoList(string strJson)
        {
            mLogWriter.Write("MC_Downloader::ParseVocInfoList (" + strJson + ")");
            VocList listVoc = new VocList();
            listVoc.listVoc = new List<VocInfo>();
            listVoc.bResult = false;
            try
            {
                JObject jObj = JObject.Parse(strJson);

                if (jObj[Constants.RESTAPI_CALL_S_RES_RESULT_CODE] == null) return listVoc;
                listVoc.nResult = (int)jObj[Constants.RESTAPI_CALL_S_RES_RESULT_CODE];
                mLogWriter.Write("MC_Downloader::ParseVocInfoList Result code : " + listVoc.nResult);
                if (listVoc.nResult != 0) return listVoc;

                if (jObj[Constants.RESTAPI_CALL_S_RES_RESULT_MSG] == null) return listVoc;
                listVoc.bResult = jObj[Constants.RESTAPI_CALL_S_RES_RESULT_MSG].ToString().Equals("SUCCESS") ? true : false;
                mLogWriter.Write("MC_Downloader::ParseVocInfoList Result bool : " + listVoc.bResult);
                if (listVoc.bResult == false) return listVoc;

                if (jObj[Constants.RESTAPI_CALL_S_RES_RESULT_RES] == null) return listVoc;
                JArray jAry = (JArray)jObj[Constants.RESTAPI_CALL_S_RES_RESULT_RES];
                Int32 idx = 0;
                foreach(JObject jOTmp in jAry)
                {
                    VocInfo vocInfo = new VocInfo();
                    vocInfo.strTenantID = jOTmp["tenantId"].ToString();
                    vocInfo.nAgentID = (int)jOTmp["agentId"];
                    vocInfo.strAgentLoginId = jOTmp["agentLoginId"].ToString();                    
                    vocInfo.strAgentDN = jOTmp["dnNo"].ToString();
                    vocInfo.strUploadDate = jOTmp["uploadDate"].ToString();
                    vocInfo.strFileName = jOTmp["fileName"].ToString();

                    mLogWriter.Write("  [" + idx + "]");
                    mLogWriter.Write("    tenantId   : " + vocInfo.strTenantID);
                    mLogWriter.Write("    agentId    : " + vocInfo.nAgentID);
                    mLogWriter.Write("    dnNo       : " + vocInfo.strAgentDN);
                    mLogWriter.Write("    uploadDate : " + vocInfo.strUploadDate);
                    mLogWriter.Write("    fileName   : " + vocInfo.strFileName);

                    listVoc.listVoc.Insert(idx++, vocInfo);
                }
            }
            catch (Exception ex)
            {
                mLogWriter.Write("MC_Downloader::ParseVocInfoList Failed to parse json. (" + ex.ToString() + ")");
                return listVoc;
            }

            return listVoc;
        }

        public int ConvertVocFile(string strVocPath, string strOutPath)
        {
            mLogWriter.Write("MC_Downloader::ConvertVocFile (" + strVocPath + " to " + strOutPath + ")");
            int nRc = Constants.CONVERT_VOC_RESULT_SUCCESS;
            try
            {
                iCnvtVocInit(mpCnvtVoc, mstrSaveDir);
                nRc = iCnvtVocConvertVoc(mpCnvtVoc, strVocPath, strOutPath, Constants.CONVERT_VOC_AUDIO_TYPE_MP3);
                mLogWriter.Write("MC_Downloader::ConvertVocFile Result Code  : " + nRc);
                mLogWriter.Write("MC_Downloader::ConvertVocFile File Path    : " + Marshal.PtrToStringAnsi(iCnvtVocGetOutFilePath(mpCnvtVoc)));
                mLogWriter.Write("MC_Downloader::ConvertVocFile File Size    : " + iCnvtVocGetOutFileSize(mpCnvtVoc));
            }
            catch(Exception ex)
            {
                mLogWriter.Write("MC_Downloader::ConvertVocFile Failed to convert voc file. (" + ex.ToString() + ")");
                nRc = Constants.CONVERT_VOC_RESULT_CONVERT_FAIL;                
            }

            return nRc;
        }

        public string CallRestApi(CallApiArgSet callApiArg)
        {
            string strRes = string.Empty;
            try
            {
                mLogWriter.Write("REST API URL : " + callApiArg.strURI);
                mLogWriter.Write("REST API Body : " + callApiArg.strBody);
                WebRequest webReq = WebRequest.Create(callApiArg.strURI);
                Stream strmData;
                webReq.Method = callApiArg.strMethod;
                webReq.ContentType = callApiArg.strContentType;
                if(callApiArg.dicHdr.Count > 0)
                {
                    foreach (KeyValuePair<string, string> items in callApiArg.dicHdr)
                    {
                        webReq.Headers[items.Key] = items.Value;
                    }
                }

                if (callApiArg.strMethod == "POST")
                {
                    if (callApiArg.strBody.Equals(string.Empty)) return strRes;
                    byte[] byteArray = Encoding.UTF8.GetBytes(callApiArg.strBody);
                    webReq.ContentLength = byteArray.Length;

                    strmData = webReq.GetRequestStream();
                    strmData.Write(byteArray, 0, byteArray.Length);
                    strmData.Close();
                }

                WebResponse webRes = webReq.GetResponse();

                using (strmData = webRes.GetResponseStream())
                {
                    StreamReader strmRd = new StreamReader(strmData);
                    strRes = strmRd.ReadToEnd();;
                }
                webRes.Close();
            }
            catch(Exception ex)
            {
                mLogWriter.Write("MC_Downloader::ConvertVocFile Failed to convert voc file. (" + ex.ToString() + ")");
                strRes = string.Empty;
            }

            return strRes;
        }

        public void CallRestApiThFunc(object arg)
        {
            mLogWriter.Write("* CallRestApiThFunc");
            CallApiArgSet stApiArg = (CallApiArgSet)arg;

            mstrRestApiRes = CallRestApi(stApiArg);
        }

        public void ConvtThFunc()
        {
            mLogWriter.Write("* ConvtThFunc");
            int nDoneCnt = 0;       // 완료한 개수
            int nProgressCnt = 0;   // 프로그레스바용 진행 개수

            // 전체 리스트 다운로드 진행 - URL 유효시간이 있어서 미리 다운 받아놓아야했지만 이제 안해도 됨.
            // 라이브러리 다운로드 함수 호출
            mLogWriter.Write("MC_Downloader::ConvtThFunc Download start.");
            for (int i = 0; i < mnTotalCnt; ++i)
            {
                DataGridVocSet dgVS = DataGridVocSet.GetInstance().ElementAt(i);

                dgVS.strVocPath = mstrSaveDir + "\\" + dgVS.strVocName;
                mLogWriter.Write("MC_Downloader::ConvtThFunc Download (" + i + ":" + dgVS.strFileName + ":" + dgVS.strVocPath + ")");
                if (dgVS.bEnable == true)
                {
                    int nRc = iDownVocDownload(mpDownVoc, mDownVocSvrIP, Constants.DOWNLOAD_VOC_N_PORT, mstrSaveDir, 
                                               dgVS.strTenantID, dgVS.nAgentID, dgVS.strAgentDN, dgVS.strUploadDate, dgVS.strFileName, dgVS.strVocName);
                    if (nRc == Constants.DOWNLOAD_VOC_RESULT_SUCCESS)
                    {
                        mLogWriter.Write("MC_Downloader::ConvtThFunc Succeed to download.");
                        eChngFStatus(i, Constants.STATUS_ITEM_S_NONE, Constants.STATUS_ITEM_S_NONE, Constants.STATUS_ITEM_S_DOWN_COMPL);
                    }
                    else
                    {
                        dgVS.bEnable = false;
                        mLogWriter.Write("MC_Downloader::ConvtThFunc Failed to download. (Error : " + nRc + ")");
                        eChngFStatus(i, Constants.STATUS_ITEM_S_NONE, Constants.STATUS_ITEM_S_NONE, Constants.STATUS_ITEM_S_DOWN_FAIL);
                    }
                }
                else
                {
                    mLogWriter.Write("MC_Downloader::ConvtThFunc Skip download.");
                    continue;
                }

                eChngProgress((++nProgressCnt / (double)mnProgressBarSize) * 100.0);
            }

            // 변환 진행
            mLogWriter.Write("MC_Downloader::ConvtThFunc Convert start.");
            for (int i = 0; i < mnTotalCnt; ++i)
            {
                DataGridVocSet dgVS = DataGridVocSet.GetInstance().ElementAt(i);
                mLogWriter.Write("MC_Downloader::ConvtThFunc Convert (" + i + ":" + dgVS.strVocPath + ")");
                if (dgVS.bEnable == true)
                {
                    if (ConvertVocFile(dgVS.strVocPath, dgVS.strAudioPath) == Constants.CONVERT_VOC_RESULT_SUCCESS)
                    {
                        ++mnFinishCnt;
                        string strResPath = Path.GetFileName(Marshal.PtrToStringAnsi(iCnvtVocGetOutFilePath(mpCnvtVoc)));
                        string strResSize = Util.GetSizeString(iCnvtVocGetOutFileSize(mpCnvtVoc));
                        mLogWriter.Write("MC_Downloader::ConvtThFunc Conversion Success. (" + strResPath + ":" + strResSize + ")");
                        eChngFStatus(i, strResPath, strResSize, Constants.STATUS_ITEM_S_CNVT_COMPL);
                    }
                    else
                    {
                        mLogWriter.Write("MC_Downloader::ConvtThFunc Failed to convert voc file.");
                        eChngFStatus(i, Constants.STATUS_ITEM_S_NONE, Constants.STATUS_ITEM_S_NONE, Constants.STATUS_ITEM_S_CNVT_FAIL);
                    }

                    try
                    {
                        File.Delete(dgVS.strVocPath);
                    }
                    catch (Exception ex)
                    {
                        mLogWriter.Write("MC_Downloader::ConvtThFunc Failed to delete voc file. (" + ex.ToString() + ")");
                    }
                }
                else
                {
                    mLogWriter.Write("MC_Downloader::ConvtThFunc Skip convert.");
                    continue;
                }

                eChnglblCnt(++nDoneCnt, mnWorkCnt);
                eChngProgress((++nProgressCnt / (double)mnProgressBarSize) * 100.0);
            }
        }

    }
}
