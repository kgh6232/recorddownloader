﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC_Downloader_WPF
{
    class LogWriter
    {
        private string mstrLogPath;
        private TextWriter mTxtWr;

        public bool Init(string strLogDir)
        {
            //if (strLogDir[strLogDir.Length - 1] == '\\')
            //    strLogDir.Remove(strLogDir.Length - 1);

            //if(!Util.MakeDirectory(strLogDir))
            //{
            //    Console.WriteLine("* LogWriter::Init Failed to make directory. ('" + strLogDir + "')");
            //    return false;
            //}

            //mstrLogPath = strLogDir + "\\MC_Downloader_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".log";
            //mTxtWr = TextWriter.Synchronized(new StreamWriter(mstrLogPath));

            return true;
        }

        public bool Write(string strLog)
        {
            //if (mTxtWr == null)
            //    return false;

            string strMsg = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + strLog;
            Console.WriteLine(strMsg);
            //try
            //{
            //    mTxtWr.WriteLine(strMsg);
            //    mTxtWr.Flush();
            //}
            //catch(Exception ex)
            //{
            //    Console.WriteLine("* LogWriter::Write Failed : ", ex.ToString());
            //    return false;
            //}

            return true;
        }

        public void Close()
        {
            //if(mTxtWr != null)
            //    mTxtWr.Close();
        }
    }
}
