﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace MC_Downloader_WPF
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class App : Application
    {
        private void Application_Start(object sender, StartupEventArgs e)
        {
            MC_Downloader mD = new MC_Downloader(e.Args);
            mD.Show();
        }
    }
}
