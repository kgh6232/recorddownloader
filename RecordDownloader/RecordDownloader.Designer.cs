﻿using System.Windows.Forms;

namespace RecordDownloader
{
    partial class MC_Downloader
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblExit = new System.Windows.Forms.Label();
            this.lblMini = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnlComplt = new System.Windows.Forms.Panel();
            this.pnlCompltDock = new System.Windows.Forms.Panel();
            this.chkboxOpenDir = new System.Windows.Forms.CheckBox();
            this.lblCompleted = new System.Windows.Forms.Label();
            this.chkboxClose = new System.Windows.Forms.CheckBox();
            this.pnlStatus = new System.Windows.Forms.Panel();
            this.lblStrCount = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.lblSplit = new System.Windows.Forms.Label();
            this.lblStrPass = new System.Windows.Forms.Label();
            this.pnlProgress = new System.Windows.Forms.Panel();
            this.pnlProgressBG = new System.Windows.Forms.Panel();
            this.btStart = new System.Windows.Forms.Button();
            this.lblTime = new System.Windows.Forms.Label();
            this.tmProgress = new System.Windows.Forms.Timer(this.components);
            this.lvList = new System.Windows.Forms.ListView();
            this.clmnEmpty = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmnFileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmnFileSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmnStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dirsch = new System.DirectoryServices.DirectorySearcher();
            this.pnlDir = new System.Windows.Forms.Panel();
            this.lblStrDir = new System.Windows.Forms.Label();
            this.tbSaveDir = new System.Windows.Forms.TextBox();
            this.btSearchDir = new System.Windows.Forms.Button();
            this.pnlComplt.SuspendLayout();
            this.pnlCompltDock.SuspendLayout();
            this.pnlStatus.SuspendLayout();
            this.pnlDir.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.BackColor = System.Drawing.Color.Black;
            this.lblExit.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(775, 5);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(20, 21);
            this.lblExit.TabIndex = 0;
            this.lblExit.Text = "X";
            this.lblExit.Click += new System.EventHandler(this.lbExit_Click);
            // 
            // lblMini
            // 
            this.lblMini.AutoSize = true;
            this.lblMini.BackColor = System.Drawing.Color.Black;
            this.lblMini.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMini.ForeColor = System.Drawing.Color.White;
            this.lblMini.Location = new System.Drawing.Point(745, 5);
            this.lblMini.Name = "lblMini";
            this.lblMini.Size = new System.Drawing.Size(20, 19);
            this.lblMini.TabIndex = 1;
            this.lblMini.Text = "_";
            this.lblMini.Click += new System.EventHandler(this.lblMini_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(5, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(135, 21);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "MC_Downloader";
            this.lblTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lblTitle_MouseDown);
            this.lblTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lblTitle_MouseMove);
            // 
            // pnlComplt
            // 
            this.pnlComplt.BackColor = System.Drawing.Color.DimGray;
            this.pnlComplt.Controls.Add(this.pnlCompltDock);
            this.pnlComplt.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlComplt.Location = new System.Drawing.Point(0, 486);
            this.pnlComplt.Name = "pnlComplt";
            this.pnlComplt.Size = new System.Drawing.Size(800, 30);
            this.pnlComplt.TabIndex = 0;
            // 
            // pnlCompltDock
            // 
            this.pnlCompltDock.BackColor = System.Drawing.Color.DimGray;
            this.pnlCompltDock.Controls.Add(this.chkboxOpenDir);
            this.pnlCompltDock.Controls.Add(this.lblCompleted);
            this.pnlCompltDock.Controls.Add(this.chkboxClose);
            this.pnlCompltDock.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlCompltDock.Location = new System.Drawing.Point(428, 0);
            this.pnlCompltDock.Name = "pnlCompltDock";
            this.pnlCompltDock.Size = new System.Drawing.Size(372, 30);
            this.pnlCompltDock.TabIndex = 3;
            // 
            // chkboxOpenDir
            // 
            this.chkboxOpenDir.AutoSize = true;
            this.chkboxOpenDir.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkboxOpenDir.ForeColor = System.Drawing.Color.Gainsboro;
            this.chkboxOpenDir.Location = new System.Drawing.Point(257, 5);
            this.chkboxOpenDir.Name = "chkboxOpenDir";
            this.chkboxOpenDir.Size = new System.Drawing.Size(110, 21);
            this.chkboxOpenDir.TabIndex = 2;
            this.chkboxOpenDir.Text = "저장폴더 열기";
            this.chkboxOpenDir.UseVisualStyleBackColor = true;
            this.chkboxOpenDir.CheckedChanged += new System.EventHandler(this.chkboxOpenDir_CheckedChanged);
            // 
            // lblCompleted
            // 
            this.lblCompleted.AutoSize = true;
            this.lblCompleted.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCompleted.ForeColor = System.Drawing.Color.White;
            this.lblCompleted.Location = new System.Drawing.Point(22, 3);
            this.lblCompleted.Name = "lblCompleted";
            this.lblCompleted.Size = new System.Drawing.Size(134, 21);
            this.lblCompleted.TabIndex = 0;
            this.lblCompleted.Text = "다운로드 완료 후";
            // 
            // chkboxClose
            // 
            this.chkboxClose.AutoSize = true;
            this.chkboxClose.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkboxClose.ForeColor = System.Drawing.Color.Gainsboro;
            this.chkboxClose.Location = new System.Drawing.Point(182, 5);
            this.chkboxClose.Name = "chkboxClose";
            this.chkboxClose.Size = new System.Drawing.Size(71, 21);
            this.chkboxClose.TabIndex = 1;
            this.chkboxClose.Text = "창 닫기";
            this.chkboxClose.UseVisualStyleBackColor = true;
            this.chkboxClose.CheckedChanged += new System.EventHandler(this.chkboxClose_CheckedChanged);
            // 
            // pnlStatus
            // 
            this.pnlStatus.BackColor = System.Drawing.Color.LightGray;
            this.pnlStatus.Controls.Add(this.lblStrCount);
            this.pnlStatus.Controls.Add(this.lblCount);
            this.pnlStatus.Controls.Add(this.lblSplit);
            this.pnlStatus.Controls.Add(this.lblStrPass);
            this.pnlStatus.Controls.Add(this.pnlProgress);
            this.pnlStatus.Controls.Add(this.pnlProgressBG);
            this.pnlStatus.Controls.Add(this.btStart);
            this.pnlStatus.Controls.Add(this.lblTime);
            this.pnlStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStatus.Location = new System.Drawing.Point(0, 401);
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(800, 85);
            this.pnlStatus.TabIndex = 0;
            // 
            // lblStrCount
            // 
            this.lblStrCount.AutoSize = true;
            this.lblStrCount.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblStrCount.Location = new System.Drawing.Point(212, 51);
            this.lblStrCount.Name = "lblStrCount";
            this.lblStrCount.Size = new System.Drawing.Size(52, 17);
            this.lblStrCount.TabIndex = 7;
            this.lblStrCount.Text = "개 완료";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCount.Location = new System.Drawing.Point(150, 39);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(70, 30);
            this.lblCount.TabIndex = 6;
            this.lblCount.Text = "00/00";
            // 
            // lblSplit
            // 
            this.lblSplit.AutoSize = true;
            this.lblSplit.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSplit.ForeColor = System.Drawing.Color.DimGray;
            this.lblSplit.Location = new System.Drawing.Point(131, 39);
            this.lblSplit.Name = "lblSplit";
            this.lblSplit.Size = new System.Drawing.Size(18, 30);
            this.lblSplit.TabIndex = 5;
            this.lblSplit.Text = "|";
            // 
            // lblStrPass
            // 
            this.lblStrPass.AutoSize = true;
            this.lblStrPass.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStrPass.Location = new System.Drawing.Point(91, 51);
            this.lblStrPass.Name = "lblStrPass";
            this.lblStrPass.Size = new System.Drawing.Size(34, 17);
            this.lblStrPass.TabIndex = 4;
            this.lblStrPass.Text = "경과";
            // 
            // pnlProgress
            // 
            this.pnlProgress.BackColor = System.Drawing.Color.RoyalBlue;
            this.pnlProgress.Location = this.pnlProgressBG.Location;
            this.pnlProgress.Name = "pnlProgress";
            this.pnlProgress.Size = new System.Drawing.Size(10, 20);
            this.pnlProgress.TabIndex = 0;
            this.pnlProgress.SizeChanged += new System.EventHandler(this.pnlProgress_SizeChanged);
            // 
            // pnlProgressBG
            // 
            this.pnlProgressBG.BackColor = System.Drawing.Color.White;
            this.pnlProgressBG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlProgressBG.Location = new System.Drawing.Point(7, 16);
            this.pnlProgressBG.Name = "pnlProgressBG";
            this.pnlProgressBG.Size = new System.Drawing.Size(650, 20);
            this.pnlProgressBG.TabIndex = 0;
            // 
            // btStart
            // 
            this.btStart.BackColor = System.Drawing.Color.Black;
            this.btStart.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btStart.FlatAppearance.BorderSize = 0;
            this.btStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btStart.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btStart.ForeColor = System.Drawing.Color.White;
            this.btStart.Location = new System.Drawing.Point(666, 9);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(121, 67);
            this.btStart.TabIndex = 1;
            this.btStart.Text = "다운로드";
            this.btStart.UseVisualStyleBackColor = false;
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTime.ForeColor = System.Drawing.Color.Black;
            this.lblTime.Location = new System.Drawing.Point(5, 39);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(95, 30);
            this.lblTime.TabIndex = 3;
            this.lblTime.Text = "00:00:00";
            // 
            // tmProgress
            // 
            this.tmProgress.Interval = 1000;
            this.tmProgress.Tick += new System.EventHandler(this.tmProgress_Tick);
            // 
            // lvList
            // 
            this.lvList.BackColor = System.Drawing.Color.White;
            this.lvList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clmnEmpty,
            this.clmnFileName,
            this.clmnFileSize,
            this.clmnStatus});
            this.lvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvList.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lvList.GridLines = true;
            this.lvList.HideSelection = false;
            this.lvList.Location = new System.Drawing.Point(0, 35);
            this.lvList.Name = "lvList";
            this.lvList.Size = new System.Drawing.Size(800, 366);
            this.lvList.TabIndex = 3;
            this.lvList.TileSize = new System.Drawing.Size(228, 28);
            this.lvList.UseCompatibleStateImageBehavior = false;
            this.lvList.View = System.Windows.Forms.View.Details;
            // 
            // clmnEmpty
            // 
            this.clmnEmpty.Text = "비어있음";
            this.clmnEmpty.Width = 0;
            // 
            // clmnFileName
            // 
            this.clmnFileName.Text = "파일 이름";
            this.clmnFileName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.clmnFileName.Width = 600;
            // 
            // clmnFileSize
            // 
            this.clmnFileSize.Text = "파일 크기";
            this.clmnFileSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.clmnFileSize.Width = 0;
            // 
            // clmnStatus
            // 
            this.clmnStatus.Text = "상태";
            this.clmnStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.clmnStatus.Width = 183;
            // 
            // dirsch
            // 
            this.dirsch.ClientTimeout = System.TimeSpan.Parse("-00:00:01");
            this.dirsch.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01");
            this.dirsch.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01");
            // 
            // pnlDir
            // 
            this.pnlDir.BackColor = System.Drawing.Color.LightGray;
            this.pnlDir.Controls.Add(this.lblStrDir);
            this.pnlDir.Controls.Add(this.tbSaveDir);
            this.pnlDir.Controls.Add(this.btSearchDir);
            this.pnlDir.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDir.Location = new System.Drawing.Point(0, 0);
            this.pnlDir.Name = "pnlDir";
            this.pnlDir.Size = new System.Drawing.Size(800, 35);
            this.pnlDir.TabIndex = 4;
            // 
            // lblStrDir
            // 
            this.lblStrDir.AutoSize = true;
            this.lblStrDir.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStrDir.Location = new System.Drawing.Point(3, 6);
            this.lblStrDir.Name = "lblStrDir";
            this.lblStrDir.Size = new System.Drawing.Size(80, 21);
            this.lblStrDir.TabIndex = 2;
            this.lblStrDir.Text = "저장 위치";
            // 
            // tbSaveDir
            // 
            this.tbSaveDir.BackColor = System.Drawing.Color.White;
            this.tbSaveDir.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbSaveDir.Location = new System.Drawing.Point(85, 5);
            this.tbSaveDir.Name = "tbSaveDir";
            this.tbSaveDir.Size = new System.Drawing.Size(620, 25);
            this.tbSaveDir.TabIndex = 1;
            this.tbSaveDir.Text = "D:/Goorme/Backup/Record";
            // 
            // btSearchDir
            // 
            this.btSearchDir.BackColor = System.Drawing.Color.Black;
            this.btSearchDir.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btSearchDir.ForeColor = System.Drawing.Color.White;
            this.btSearchDir.Location = new System.Drawing.Point(715, 6);
            this.btSearchDir.Name = "btSearchDir";
            this.btSearchDir.Size = new System.Drawing.Size(75, 23);
            this.btSearchDir.TabIndex = 0;
            this.btSearchDir.Text = "폴더 변경";
            this.btSearchDir.UseVisualStyleBackColor = false;
            this.btSearchDir.EnabledChanged += new System.EventHandler(this.btSearchDir_EnabledChanged);
            this.btSearchDir.Click += new System.EventHandler(this.btSearchDir_Click);
            // 
            // MC_Downloader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(800, 516);
            this.Controls.Add(this.lvList);
            this.Controls.Add(this.pnlDir);
            this.Controls.Add(this.pnlStatus);
            this.Controls.Add(this.pnlComplt);
            this.Controls.Add(this.lblExit);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblMini);
            this.MaximizeBox = false;
            this.Name = "MC_Downloader";
            this.Text = "MC_Downloader";
            this.Load += new System.EventHandler(this.Main_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Main_MouseMove);
            this.pnlComplt.ResumeLayout(false);
            this.pnlCompltDock.ResumeLayout(false);
            this.pnlCompltDock.PerformLayout();
            this.pnlStatus.ResumeLayout(false);
            this.pnlStatus.PerformLayout();
            this.pnlDir.ResumeLayout(false);
            this.pnlDir.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label lblMini;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel pnlComplt;
        private System.Windows.Forms.Panel pnlStatus;
        private System.Windows.Forms.Button btStart;
        private System.Windows.Forms.Panel pnlProgressBG;
        private System.Windows.Forms.Panel pnlProgress;
        private System.Windows.Forms.Timer tmProgress;
        private Label lblCompleted;
        private CheckBox chkboxClose;
        private CheckBox chkboxOpenDir;
        private Label lblTime;
        private Label lblStrPass;
        private Label lblSplit;
        private Label lblCount;
        private Label lblStrCount;
        private ListView lvList;
        private ColumnHeader clmnFileSize;
        private ColumnHeader clmnStatus;
        private System.DirectoryServices.DirectorySearcher dirsch;
        private Panel pnlDir;
        private Button btSearchDir;
        private TextBox tbSaveDir;
        private Label lblStrDir;
        private Panel pnlCompltDock;
        private ColumnHeader clmnEmpty;
        private ColumnHeader clmnFileName;
    }
}

