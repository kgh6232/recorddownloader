﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace RecordDownloader
{
    public partial class MC_Downloader : Form
    {
        public MC_Downloader(string[] args)
        {
            Console.WriteLine("* Main");
            InitializeComponent();

            Init(args);

            eChnglblCnt += new ChnglblCnt(SetLblCountText);
            eChngFStatus += new ChngFStatus(SetFStatus);
            eChngProgress += new ChngProgress(SetProgress);
        }

        private void Main_Load(object sender, EventArgs e)
        {
            Console.WriteLine("* Main_Load");
            tbSaveDir.Text = strSaveDir;

            btSearchDir.TabStop = false;
            btSearchDir.FlatStyle = FlatStyle.Flat;
            btSearchDir.FlatAppearance.BorderSize = 0;

            btStart.TabStop = false;
            btStart.FlatStyle = FlatStyle.Flat;
            btStart.FlatAppearance.BorderSize = 0;

            pnlProgress.Location = pnlProgressBG.Location;
            pnlProgress.Width = 0;

            lblCount.Text = String.Format("00/{0:D2}", nTotCnt);

            lvList.BeginUpdate();
            ListViewItem lvItem;
            for (int i=0; i<strUrlList.Length; ++i)
            {
                Console.WriteLine("**    : " + strUrlList[i]);
                lvItem = new ListViewItem("");
                lvItem.SubItems.Add(strUrlList[i]);
                lvItem.SubItems.Add("");
                lvItem.SubItems.Add("");
                lvList.Items.Add(lvItem);
            }
            lvList.EndUpdate();
        }

        private void lbExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Main_MouseDown(object sender, MouseEventArgs e)
        {
            pMousePoint = new Point(e.X, e.Y);
        }

        private void Main_MouseMove(object sender, MouseEventArgs e)
        {
            if((e.Button & MouseButtons.Left) == MouseButtons.Left)
                Location = new Point(this.Left - (pMousePoint.X - e.X), this.Top - (pMousePoint.Y - e.Y));
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            pMousePoint = new Point(e.X, e.Y);
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
                Location = new Point(this.Left - (pMousePoint.X - e.X), this.Top - (pMousePoint.Y - e.Y));
        }

        private void btStart_Click(object sender, EventArgs e)
        {
            if (nStatus == Constants.STATUS_N_NONE)
            {
                if(!MakeDirectory(strSaveDir))
                {
                    MessageBox.Show("디렉토리를 생성할 수 없습니다.");
                    return;
                }
                tmProgress.Start();
                nStatus = Constants.STATUS_N_START;
                btStart.Text = "진행 중";

                hTh.Start();
                btSearchDir.Enabled = false;
            }
            else if (nStatus == Constants.STATUS_N_START)
            {
                nStatus = Constants.STATUS_N_PAUSE;
                btStart.Text = "일시 정지";
                tmProgress.Stop();
            }
            else if (nStatus == Constants.STATUS_N_PAUSE)
            {
                nStatus = Constants.STATUS_N_START;
                btStart.Text = "진행 중";
                tmProgress.Start();
            }
            else
            {
            }
        }

        private void tmProgress_Tick(object sender, EventArgs e)
        {
            ++nTimer;
            lblTime.Text = GetTimeString(nTimer);
        }

        private void chkboxClose_CheckedChanged(object sender, EventArgs e)
        {
            bCloseWindow = true;
        }

        private void chkboxOpenDir_CheckedChanged(object sender, EventArgs e)
        {
            bOpenDir = true;
        }

        private void pnlProgress_SizeChanged(object sender, EventArgs e)
        {
            if(pnlProgress.Width >= pnlProgressBG.Width)
            {
                pnlProgress.Width = pnlProgressBG.Width;
                tmProgress.Stop();
                nStatus = Constants.STATUS_N_DONE;
                btStart.Text = "완료";
                if (bOpenDir == true)
                    Process.Start(strSaveDir);
                if (bCloseWindow == true)
                    this.Close();
            }
        }

        private void btSearchDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowDialog();

            if (fbd.SelectedPath.Length > 0)
            {
                strSaveDir = fbd.SelectedPath;
                tbSaveDir.Text = strSaveDir;
            }
        }

        private void SetLblCountText(int nCnt)
        {
            lblCount.Text = String.Format("{0:D2}/{1:D2}", nCnt, nTotCnt);
        }

        private void SetFStatus(int nIdx, string strStatus)
        {
            lvList.Items[nIdx].SubItems[3].Text = strStatus;
        }

        private void SetProgress(double nPercent)
        {
            pnlProgress.Width = (int)(pnlProgressBG.Width * nPercent / 100);
        }

        private void btSearchDir_EnabledChanged(object sender, EventArgs e)
        {
            if(btSearchDir.Enabled == false)
            {
                btSearchDir.BackColor = Color.SlateGray;
            }
        }
    }
}
