﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace RecordDownloader
{
    static class Constants
    {
        /* Status */
        public const int STATUS_N_NONE  = 0;
        public const int STATUS_N_START = 1;
        public const int STATUS_N_PAUSE = 2;
        public const int STATUS_N_DONE  = 3;
        public const int STATUS_N_ERROR = 99;

        /*  Convert Voc File Result*/
        public const int CONVERT_VOC_RESULT_SUCCESS = 0;
        public const int CONVERT_VOC_RESULT_CODEC_ERROR = 1;
        public const int CONVERT_VOC_RESULT_CANNOT_OPEN_VOC = 2;
        public const int CONVERT_VOC_RESULT_CANNOT_FIND_CODEC = 3;
        public const int CONVERT_VOC_RESULT_CANNOT_OPEN_DST_FILE = 4;
        public const int CONVERT_VOC_RESULT_READ_FAIL = 5;
        public const int CONVERT_VOC_RESULT_CONVERT_FAIL = 6;
        public const int CONVERT_VOC_RESULT_WRITE_FAIL = 7;
        public const int CONVERT_VOC_RESULT_UNKNOWN = 99;

        /* Codec */

        public const int CONVERT_VOC_AUDIO_TYPE_MP3 = 0;
        public const int CONVERT_VOC_AUDIO_TYPE_PCM = 1;
        public const int CONVERT_VOC_AUDIO_TYPE_ALAW = 2;
        public const int CONVERT_VOC_AUDIO_TYPE_G723_1 = 3;
    }

    public struct VocSet
    {
        public string strUrl { get; set; }
        public string strVocPath { get; set; }
        public string strResPath { get; set; }
        public ulong nResSize { get; set; }
    }

    partial class MC_Downloader
    {
        /* ConvertVoc.dll Functions */
        [DllImport("ConvertVoc.dll")]
        extern public static IntPtr iCnvtVocConstruct();
        [DllImport("ConvertVoc.dll")]
        extern public static void iCnvtVocInit(IntPtr pObj, string szOutDir);
        [DllImport("ConvertVoc.dll")]
        extern public static void iCnvtVocClear(IntPtr pObj);
        [DllImport("ConvertVoc.dll")]
        extern public static Int32 iCnvtVocConvertVoc(IntPtr pObj, string szVocFile, UInt16 nAudioType);
        [DllImport("ConvertVoc.dll")]
        extern public static IntPtr iCnvtVocGetOutFilePath(IntPtr pObj);
        [DllImport("ConvertVoc.dll")]
        extern public static UInt64 iCnvtVocGetOutFileSize(IntPtr pObj);

        /* 변수 */
        public IntPtr   pCnvtVoc;       // iCnvtVoc dll 객체 저장
        public int      nStatus;        // 상태 (0:none / 1:start / 2:pause / 3:Done / 99:Error)
        public int      nTotCnt;        // 전체 파일 개수
        public int      nFinishCnt;     // 변환까지 완료한 파일 개수
        public string   strStartDate;   // 녹취 선택 시작 날짜
        public string   strEndDate;     // 녹취 선택 끝 날짜
        public VocSet[] stVocList;      // Voc 변환 경과가 담길 구조체 배열
        public string[] strUrlList;     // 다운 받을 URL 목록
        public string   strSaveDir;     // 파일 저장 디렉토리
        public int      nProgress;      // 진행 %
        public bool     bOpenDir;       // 완료 후 디렉토리 오픈 여부
        public bool     bCloseWindow;   // 완료 후 창 종료 여부
        public int      nTimer;         // 진행 시간
        public Point    pMousePoint;    // 마우스 좌표

        public Thread   hTh;            // 스레드
        public delegate void ChnglblCnt(int nCnt);  // lblCount text 변경 용 델리게이트
        public event ChnglblCnt eChnglblCnt;        // lblCount text 변경 용 델리게이트 이벤트
        public delegate void ChngFStatus(int nIdx, string strStatus);   // lvList 변경 용 델리게이트
        public event ChngFStatus eChngFStatus;      // lvList 변경 용 델리게이트 이벤트
        public delegate void ChngProgress(double nPercent);   // pnlProgress 변경 용 델리게이트
        public event ChngProgress eChngProgress;    // pnlProgress 변경 용 델리게이트 이벤트

        /// <summary>
        /// 해당 애플리케이션의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("* Main");
            for (int i = 0; i < args.Length; ++i)
            {
                Console.WriteLine("  > [" + i + "] : " + args[i]);
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MC_Downloader(args));
        }

        public void Init(string[] args)
        {
            Console.WriteLine("* Program::Init");
            if (args.Length > 0)
            {
                strUrlList = GetVocUrlList(args[0]);    // parse Json argument
            }
            nTotCnt = strUrlList.Length;
            stVocList = new VocSet[nTotCnt];
            Console.WriteLine("  > arguments");
            Console.WriteLine("    - Start date : " + strStartDate);
            Console.WriteLine("    - End date   : " + strEndDate);
            for (int i=0; i<nTotCnt; ++i)
            {
                VocSet stVocSet = new VocSet();
                stVocSet.strUrl = strUrlList[i];
                stVocList.SetValue(stVocSet, i);
                Console.WriteLine(string.Format("    - {0:D2} : {1}", i, stVocSet.strUrl));
            }

            pCnvtVoc = iCnvtVocConstruct();

            nStatus = Constants.STATUS_N_NONE;
            strSaveDir = "C:\\Users\\" + Environment.UserName + "\\Documents\\MC_Downloader";
            nProgress = 0;

            hTh = new Thread(ThRun);
        }

        public string[] GetVocUrlList(string strJson)
        {
            JObject jObj = JObject.Parse(strJson);
            strStartDate = jObj["START_DATE"].ToString();
            strEndDate = jObj["END_DATE"].ToString();
            JArray jArr = (JArray)jObj["LIST"];
            string[] strList = new string[jArr.Count];
            int idx = 0;
            foreach (JValue value in jArr.Children())
            {
                Console.WriteLine(value.Value<string>().ToString());
                strList.SetValue(value.Value<string>().ToString(), idx++);
            }

            return strList;
        }

        public bool MakeDirectory(string strDir)
        {
            DirectoryInfo dInfo = new DirectoryInfo(strDir);
            if (dInfo.Exists == false)
            {
                dInfo.Create();
                if (dInfo.Exists == false)
                {
                    return false;
                }
            }

            return true;
        }

        public string VocFileDownload(string strUrl)
        {
            if (strUrl.Length <= 0) return "";

            Uri uri = new Uri(strUrl);
            string strFileName = uri.Segments[uri.Segments.Length - 1];
            if (strFileName.Length <= 0) return "";
            string strDstPath = strSaveDir + "\\" + strFileName;

            try
            {
                WebClient webClient = new WebClient();
                webClient.DownloadFile(new Uri(strUrl), strDstPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Program::VocFileDownload - " + ex);
                return "";
            }

            return strDstPath;
        }

        public int ConvertVocFile(string strVocFile)
        {
            int nRc = Constants.CONVERT_VOC_RESULT_SUCCESS;
            iCnvtVocInit(pCnvtVoc, strSaveDir);
            nRc = iCnvtVocConvertVoc(pCnvtVoc, strVocFile, Constants.CONVERT_VOC_AUDIO_TYPE_PCM);
            Console.WriteLine("** Result   : " + nRc);
            Console.WriteLine("** File     : " + Marshal.PtrToStringAnsi(iCnvtVocGetOutFilePath(pCnvtVoc)));
            Console.WriteLine("** FileSize : " + iCnvtVocGetOutFileSize(pCnvtVoc));
            iCnvtVocClear(pCnvtVoc);

            return nRc;
        }

        public void ThRun()
        {
            Console.WriteLine("* ListView");
            for (int i = 0; i < nTotCnt; ++i)
            {
                VocSet stVocSet = (VocSet)stVocList.GetValue(i);
                string strVocFile = VocFileDownload(stVocSet.strUrl);
                if (strVocFile.Length > 0)
                {
                    stVocSet.strVocPath = strVocFile;
                    eChngFStatus(i, "다운로드 완료");
                    if (ConvertVocFile(strVocFile) == Constants.CONVERT_VOC_RESULT_SUCCESS)
                    {
                        ++nFinishCnt;
                        stVocSet.strResPath = Marshal.PtrToStringAnsi(iCnvtVocGetOutFilePath(pCnvtVoc));
                        stVocSet.nResSize = iCnvtVocGetOutFileSize(pCnvtVoc);
                        eChngFStatus(i, "변환 완료");
                    }
                    else
                    {
                        eChngFStatus(i, "변환 실패");
                    }
                }
                else
                {
                    eChngFStatus(i, "다운로드 실패");
                }
                stVocList.SetValue(stVocSet, i);

                eChnglblCnt(i+1);
                eChngProgress(((i + 1) / (double)nTotCnt) * 100.0);
            }
        }

        public string GetTimeString(int nTime)
        {
            int nHour = nTime / 3600;
            int nMin = (nTime % 3600) / 60;
            int nSec = nTime % 60;

            return String.Format("{0:D2}:{1:D2}:{2:D2}", nHour, nMin, nSec);
        }
    }
}
